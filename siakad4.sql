-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Okt 2022 pada 03.31
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siakad4`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `id` int(255) NOT NULL,
  `nidn` int(50) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `alamat` varchar(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL,
  `id_user` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`id`, `nidn`, `nama`, `jenis_kelamin`, `no_hp`, `email`, `alamat`, `is_active`, `id_user`) VALUES
(6, 1001, 'Elviwani', 'perempuan', '082217919404', 'm2319f2779@bangkit.a', 'medan', '1', 39),
(7, 1002, 'Ade Chandra', 'laki-laki', '082217919404', 'm2319f2779@bangkit.a', 'medan', '1', 40),
(8, 1003, 'juju', 'laki-laki', '082217919404', 'amirulilmi20@gmail.c', 'medan', '1', 48);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(255) NOT NULL,
  `nim` int(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `id_orangtua` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `created_by` int(255) NOT NULL,
  `updated_at` timestamp(6) NULL DEFAULT current_timestamp(6),
  `updated_by` int(255) DEFAULT NULL,
  `id_dosen` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nim`, `nama`, `email`, `jenis_kelamin`, `alamat`, `no_hp`, `id_orangtua`, `id_user`, `is_active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `id_dosen`) VALUES
(69, 191401009, 'febriando manik', 'amirulilmi20@gmail.c', 'laki-laki', 'medan', '082217919404', 60, 42, '1', '2022-10-03 00:51:26.083383', 0, NULL, NULL, 8),
(70, 191401009, 'Hanif Misbah', 'amirulilmi20@gmail.c', 'laki-laki', 'medan', '082217919404', 61, 43, '1', '2022-10-03 00:50:54.278176', 0, NULL, NULL, 7),
(71, 191401099, 'M Amirul Ilmii', 'amirulilmi20@gmail.com', 'laki-laki', 'medan', '082217919404', 62, 44, '1', '2022-09-30 01:24:23.183637', 0, NULL, NULL, 5),
(73, 191401003, 'abid', 'amirulilmi20@gmail.com', 'laki-laki', 'lautdendang', '082217919404', 67, 52, '1', '2022-09-30 01:41:25.224998', 0, '2022-09-30 01:41:25.224998', NULL, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orangtua`
--

CREATE TABLE `orangtua` (
  `id_orangtua` int(255) NOT NULL,
  `nama_ayah` varchar(20) NOT NULL,
  `no_hp_ayah` varchar(20) NOT NULL,
  `nama_ibu` varchar(20) NOT NULL,
  `no_hp_ibu` int(20) NOT NULL,
  `alamat_ortu` varchar(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `orangtua`
--

INSERT INTO `orangtua` (`id_orangtua`, `nama_ayah`, `no_hp_ayah`, `nama_ibu`, `no_hp_ibu`, `alamat_ortu`, `is_active`) VALUES
(60, 'Irson Amir', '082217919404', 'Erna Yanti', 2147483647, 'Jl. Dr. Mansyur No.5', '1'),
(61, 'Irson Amir', '082217919404', 'Erna Yanti', 2147483647, 'Jl. Dr. Mansyur No.5', '1'),
(62, 'Irson Amir', '082217919404', 'Erna Yanti', 2147483647, 'Jl. Dr. Mansyur No.5', '1'),
(67, 'Irson Amir', '082217919404', 'Erna Yanti', 2147483647, 'Jl. Dr. Mansyur No.5', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `real_name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` enum('1','0') NOT NULL,
  `id_group` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `real_name`, `email`, `password`, `last_login_at`, `is_active`, `id_group`) VALUES
(2, 'admin123', 'Elviwani', 'elviwani20@gmail.com', '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9', '2022-09-22 01:32:22', '1', 1),
(39, '1001', 'Elviwani', 'm2319f2779@bangkit.academy', 'fe675fe7aaee830b6fed09b64e034f84dcbdaeb429d9cccd4ebb90e15af8dd71', '2022-09-26 04:47:53', '1', 3),
(40, '1002', 'Ade Chandra', 'm2319f2779@bangkit.academy', 'b281bc2c616cb3c3a097215fdc9397ae87e6e06b156cc34e656be7a1a9ce8839', '2022-09-26 04:48:24', '1', 3),
(42, '191401009', 'Febriando Manik', 'kali1@gmail.com', '3fe400f55ef7e7c9c6b8ad05794211316e098c76f651bbd17b72266fc403526d', '2022-09-26 04:49:57', '1', 4),
(43, '191401105', 'Hanif Misbah', 'm2319f2779@bangkit.academy', '1e41d7828409164e81af3bc03f12e68beb29984ed80fc93e545beef4114c787b', '2022-09-26 04:50:25', '1', 4),
(44, '191401099', 'M Amirul Ilmi', 'amirulilmi20@gmail.com', '2723bb5c1881c80affd08181feaf993c3f4e889c437163fa412970a1d39166da', '2022-09-26 05:17:07', '1', 4),
(45, 'adminBAAK123', 'adminBAAK123', 'adminbaak20@gmail.com', '252389aca5b0a7110d6b5a48ed35c57369a834c331cd0dab7b23c4c6424e71b5', '2022-09-26 05:17:45', '1', 2),
(48, '1003', 'juju', 'amirulilmi20@gmail.com', '8c9a013ab70c0434313e3e881c310b9ff24aff1075255ceede3f2c239c231623', '2022-09-30 01:36:57', '1', 3),
(49, '123456', 'tes', 'amirulilmi20@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2022-09-30 01:38:12', '1', 2),
(52, '191401003', 'abid', 'amirulilmi20@gmail.com', 'b0f55ce6a0989afbd5617249c1cf26baf99a6e0d74b630b1ed3eaaf364c0207a', '2022-09-30 01:41:25', '1', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_group`
--

CREATE TABLE `user_group` (
  `id_group` int(255) NOT NULL,
  `nama_group` varchar(20) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `dbusername` varchar(20) NOT NULL,
  `is_active` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_group`
--

INSERT INTO `user_group` (`id_group`, `nama_group`, `keterangan`, `dbusername`, `is_active`) VALUES
(1, 'Administrator', 'CRUD semua mahasiswa', '', '1'),
(2, 'Admin BAAK', 'CRU semua mahasiswa', '', '1'),
(3, 'Dosen', 'RU mahasiswa bimbingan', '', '1'),
(4, 'Mahasiswa', 'RU dirinya sendiri', '', '1');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_orangtua` (`id_orangtua`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `orangtua`
--
ALTER TABLE `orangtua`
  ADD PRIMARY KEY (`id_orangtua`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`);

--
-- Indeks untuk tabel `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id_group`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT untuk tabel `orangtua`
--
ALTER TABLE `orangtua`
  MODIFY `id_orangtua` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id_group` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `dosen_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`id_orangtua`) REFERENCES `orangtua` (`id_orangtua`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
