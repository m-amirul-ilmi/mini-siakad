<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mahasiswa extends CI_Model {

	public function getdata()
	{
      $this->db->select('*');
      $this->db->from('mahasiswa','left');
      $this->db->join('orangtua','orangtua.id_orangtua = mahasiswa.id_orangtua','left');  
    //   $this->db->join('dosen','mahasiswa.id_dosen = dosen.id','left');    
      $query = $this->db->get();
      return $query->result_array();

	}
    public function getdata_dosen()
	{
       return $this->db->get('dosen')->result_array();
	}
    public function getdata_baak()
	{
       $id_group = 2;
       return $this->db->get_where('user',['id_group'=>$id_group])->result_array();
                 //get_where('user',['id'=>$id_user])
	}
    public function getdata_dosen_bimbingan()//mengambil id dosen bimbingan
	{
       $id_user = $this->session->userdata('id_user');
       return $this->db->get_where('dosen',['id_user'=>$id_user])->row_array();
                 //get_where('user',['id'=>$id_user])
	}
    public function getdata_mahasiswa_bimbingan($id_dosen)
	{
       return $this->db->get_where('mahasiswa',['id_dosen'=>$id_dosen])->result_array();
                 //get_where('user',['id'=>$id_user])
	}
    

   

    //mengambil data nidn dan diubah menjadi id dosen untuk halaman tambah mahasiswa
    public function getdata_nidn($nidn)
	{
        return $this->db->get_where('dosen',['nidn'=>$nidn])->row_array();
	}

    //mengambil detail untuk di halaman edit
    public function mahasiswa_detail($id)
    {
        return $this->db->get_where('mahasiswa',['id'=>$id])->row_array();
    //   $this->db->select('*');
    //   $this->db->from('mahasiswa');
    //   $this->db->join('orangtua','orangtua.id_orangtua = mahasiswa.id_orangtua');
    //   $this->db->join('dosen','dosen.id = mahasiswa.id_dosen') ;
    //   $this->db->join('dosen','mahasiswa.id_dosen = dosen.id','left');    
    //   $query = $this->db->get();
    //   return $query->row_array();
          
    }
    public function orangtua_detail($id_orangtua)
    {
      return $this->db->get_where('orangtua',['id_orangtua'=>$id_orangtua])->row_array();
    }
    public function dosen_detail($id)
    {
      return $this->db->get_where('dosen',['id'=>$id])->row_array();       
    }
    public function baak_detail($id)
    {
      return $this->db->get_where('user',['id'=>$id])->row_array();       
    }
    

    public function proses_tambah_data($data_dosen)
    {
        $id_group = $this->input->post('id_group');

        $this->db->trans_start();

        $nim = $this->input->post('nim');
        $password = hash("sha256",$nim);
       
        $data_akun =[
			"username" => $this->input->post('nim'),
            "password" => $password,
			"real_name" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "id_group" => $id_group,
		];
        $this->db->insert('user', $data_akun);
        $last_id_user = $this->db->insert_id();
        
        $data_ortu =[
            "nama_ayah" => $this->input->post('nama_ayah'),
			"no_hp_ayah" => $this->input->post('no_hp_ayah'),
			"nama_ibu" => $this->input->post('nama_ibu'),
            "no_hp_ibu" => $this->input->post('no_hp_ibu'),
            "alamat_ortu" => $this->input->post('alamat_ortu'),
		];
        $this->db->insert('orangtua', $data_ortu);

        $last_id = $this->db->insert_id();

        $data =[
            "nim" => $this->input->post('nim'),
			"nama" => $this->input->post('nama'),
			"email" => $this->input->post('email'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "alamat" => $this->input->post('alamat'),
			"no_hp" => $this->input->post('no_hp'),
            "id_orangtua" => $last_id, 
            "id_dosen" => $data_dosen['id'],
            "id_user" => $last_id_user,  
		];
        $this->db->insert('mahasiswa', $data);
 
        

        $this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
		}
		else{
            $this->db->trans_commit();
		}
    }

    public function proses_tambah_data_dosen()
    {

        $id_group = $this->input->post('id_group');

        $nidn = $this->input->post('nidn');
        $password = hash("sha256",$nidn);

        $this->db->trans_start();
        $data_akun =[
			"username" => $this->input->post('nidn'),
            "password" => $password,
			"real_name" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "id_group" => $id_group,
		];
        $this->db->insert('user', $data_akun);
        $last_id_user = $this->db->insert_id();

        $data_dosen =[
            "nidn" => $this->input->post('nidn'),
			"nama" => $this->input->post('nama'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "no_hp" => $this->input->post('no_hp'),
			"email" => $this->input->post('email'),
            // "id_user" => $last_id_user,  
            "alamat" => $this->input->post('alamat'),
		];
        $this->db->insert('dosen', $data_dosen);

        $this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
            $this->db->trans_commit();
		}
    }

    public function proses_tambah_data_baak()
    {

        $id_group = $this->input->post('id_group');

        $username = $this->input->post('username');
        $password = hash("sha256",$username);

        $this->db->trans_start();
        $data_akun =[
			"username" => $this->input->post('username'),
            "password" => $password,
			"real_name" => $this->input->post('real_name'),
            "email" => $this->input->post('email'),
            "id_group" => $id_group,
		];
        $this->db->insert('user', $data_akun);


        $this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
            $this->db->trans_commit();
		}
    }

    public function proses_edit_data($id_orangtua,$data_mahasiswa,$data_orangtua)
    {       
        $this->db->trans_start();
        
        $this->db->from('orangtua');
        $this->db->where('id_orangtua',$id_orangtua);
        $this->db->update('orangtua',$data_orangtua);

        $this->db->from('mahasiswa');
        $this->db->where('id_orangtua',$id_orangtua);
        $this->db->update('mahasiswa',$data_mahasiswa);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
            $this->db->trans_commit();
		}
        
    }

    public function proses_edit_dosen($id,$data_dosen)
    {       
        $this->db->from('dosen');
        $this->db->where('id',$id);
        $this->db->update('dosen',$data_dosen);
    }

    public function proses_edit_baak($id,$data_baak)
    {       
        $this->db->from('user');
        $this->db->where('id',$id);
        $this->db->update('user',$data_baak);
    }

	public function fungsi_hapus($id,$id_orangtua,$id_user)
    {

       $this->db->trans_start();
       
       $this->db->where('id_orangtua',$id_orangtua);
       $this->db->delete('orangtua');

       $this->db->from('mahasiswa');
       $this->db->where('id',$id);
       $this->db->delete('mahasiswa');

       $this->db->from('user');
       $this->db->where('id',$id_user);
       $this->db->delete('user');
        // cara menghapus lebih singkat
        //    $this->db->delete('user',['id_group'=>$id_group]);
       
       $this->db->trans_complete();
       if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
       }
        else{
            $this->db->trans_commit();
        }
       
    }

    public function fungsi_hapus_dosen($id,$id_user)
    {

       $this->db->trans_start();
       
       $this->db->from('dosen');
       $this->db->where('id',$id);
       $this->db->delete('dosen');

       $this->db->from('user');
       $this->db->where('id',$id_user);
       $this->db->delete('user');
  
       $this->db->trans_complete();
       if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
       
    }
    public function fungsi_hapus_baak($id)
    {
       $this->db->from('user');
       $this->db->where('id',$id);
       $this->db->delete('user');   
    }

    public function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	}
    public function cek_profil($table,$where){		
		return $this->db->get_where($table,$where);
	}	
   
    
}
