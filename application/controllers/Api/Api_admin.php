<?php 
 defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Api/Auth.php';
 
use Restserver\Libraries\REST_Controller;


class Api_admin extends Auth{
 
 function __construct()
        {
        // Construct the parent class
        parent::__construct();

        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        }

        public function index_get()
        {
            if ($this->authtoken() == 'salah') {
                return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
                die();
            }
    
            $id = $this->get('id');
    
            // If the id parameter doesn't exist return all the users
    
            if ($id === NULL)
            {

                // $array = array('id_group' =>1,2);
                // $users = $this->db->get('user')->result_array();
                $this->db->where_in('id_group',array('1','2'));
                $users = $this->db->get('user')->result_array();
                // Check if the users data store contains users (in case the database result returns NULL)
                if ($users)
                {
                    // Set the response and exit
                    $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
                else
                {
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No users were found'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
    
            //Find and return a single record for a particular user.
            else {
                $id = (int) $id;
    
                // Validate the id.
                if ($id <= 0)
                {
                    // Invalid id, set the response and exit.
                    $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                }
               
                    $this->db->where(array("id"=>$id));
                    $users = $this->db->get('user')->row_array();
    
                    // Set the response and exit
                    $this->response($users, REST_Controller::HTTP_OK); 
               
            }
        }
    
        public function index_post()
        {
            if ($this->authtoken() == 'salah') {
                return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
                die();
            }

            $username = $this->input->post('username');
            $password = hash("sha256",$username);

            $this->db->trans_start();
            $data_akun =[
                "username" => $this->input->post('username'),
                "password" => $password,
                "real_name" => $this->input->post('real_name'),
                "email" => $this->input->post('email'),
                "id_group" => $this->input->post('id_group'),
            ];
            $this->db->insert('user', $data_akun);
    
            $this->set_response($data_akun, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        }
    

        public function index_delete()
        {
            if ($this->authtoken() == 'salah') {
                return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
                die();
            }
            $id =$this->delete('id');
    
            // Validate the id.
            if ($id <= 0)
            {
                // Set the response and exit
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }
    
            // $this->some_model->delete_something($id);
            $where = [
                'id' => $id  
            ];
            $this->db->delete("user",$where);
            $message= array("status" =>"data berhasil dihapus");
    
            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
        }
        public function index_put()
        {
            if ($this->authtoken() == 'salah') {
                return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
                die();
            }
            // $id =$this->delete('id');
    
            // Validate the id.
            // if ($id <= 0)
            // {
            //     // Set the response and exit
            //     $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            // }
    
            // $this->some_model->delete_something($id);
            $where = [
                'id' => $this->put('id')  
            ];
            $data = array(
                "nama" => $this->put("user")
    
            );
    
            $this->db->update("user",$data,$where);
    
            $this->set_response($data, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
        }



    }
?>