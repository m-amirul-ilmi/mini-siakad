<?php
defined('BASEPATH') or exit('No direct script access allowed');


require_once APPPATH . 'controllers/Api/Auth.php';
// require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;


class Api_mahasiswa extends Auth
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }

    }


    public function index_get()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        
        if(array_key_exists('Authorization',$this->input->request_headers())==false){ //kalo gadak key token

            return $this->response('Token Tidak Ada', 401 );
          
           
           die();

        }
        
        
        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL) {
            $users = $this->db->get('mahasiswa')->result_array();
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users) {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        //Find and return a single record for a particular user.
        else {
            $id = (int) $id;

            // Validate the id.
            if ($id <= 0) {
                // Invalid id, set the response and exit.
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            
            $this->db->where(array("id" => $id));
            $users = $this->db->get('mahasiswa')->row_array();

            // Set the response and exit
            $this->response($users, REST_Controller::HTTP_OK);

 
        }
    }

    public function index_post()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        $id_group = 4;
        // $this->some_model->update_user( ... );
        $nim = $this->put('nim');
        $password = hash("sha256",$nim);
        $data_akun =[
			"username" => $this->input->post('nim'),
            "password" => $password,
			"real_name" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "id_group" => $id_group,
		];
        $this->db->insert('user', $data_akun);
        $last_id_user = $this->db->insert_id();

        $data_ortu = [
            "nama_ayah" => $this->input->post('nama_ayah'),
            "no_hp_ayah" => $this->input->post('no_hp_ayah'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "no_hp_ibu" => $this->input->post('no_hp_ibu'),
            "alamat_ortu" => $this->input->post('alamat_ortu'),
        ];

        $this->db->insert('orangtua', $data_ortu);
        $last_id = $this->db->insert_id();

        $data = [
            "nim" => $this->input->post('nim'),
            "nama" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "alamat" => $this->input->post('alamat'),
            "no_hp" => $this->input->post('no_hp'),
            "id_orangtua" => $last_id,
            // "id_dosen" => $data_dosen['id'],
            "id_user" => $last_id_user,  
        ];


        $this->db->insert("mahasiswa", $data);

        $this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }


    public function index_delete()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        $id = $this->delete('id');

        $cek =  $this->db->get_where('mahasiswa',['id'=>$id])->num_rows();
        
        // print_r($id);exit;
        // Validate the id.
        if ($cek > 0) {
            // Set the response and exit
            $where = [
                'id' => $id
            ];
            $this->db->where('id',$id);
            $this->db->delete('mahasiswa');   
            $this->db->delete('mahasiswa', $where);
            $message = array("status" => "data berhasil dihapus");

            $this->set_response($message, REST_Controller::HTTP_OK); 
  
        }

        else{

            $message = array("status" => "data tidak berhasil dihapus");
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST); 
        }
    }



    public function index_put()
    {

        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }

        $id = $this->put('id');

        $cek =  $this->db->get_where('mahasiswa',['id'=>$id])->num_rows();
        // print_r($id);exit;
        if ($cek > 0) {
            $tes =  $this->db->get_where('mahasiswa',['id'=>$this->put('id')])->row_array();
            $id_orangtua = $tes['id_orangtua'];

            $data_ortu = [
                "nama_ayah" => $this->put('nama_ayah'),
                "no_hp_ayah" => $this->put('no_hp_ayah'),
                "nama_ibu" => $this->put('nama_ibu'),
                "no_hp_ibu" => $this->put('no_hp_ibu'),
                "alamat_ortu" => $this->put('alamat_ortu'),
            ];
            $this->db->where('id_orangtua',$id_orangtua);
            $this->db->update('orangtua', $data_ortu);
            
            $data = [
                "nim" => $this->put('nim'),
                "nama" => $this->put('nama'),
                "email" => $this->put('email'),
                "jenis_kelamin" => $this->put('jenis_kelamin'),
                "alamat" => $this->put('alamat'),
                "no_hp" => $this->put('no_hp'),
                "id_orangtua" => $id_orangtua,
                // "id_dosen" => $data_dosen['id'],
                "id_user" => 4,  
            ];
    
            $this->db->where('id',$id);
            $this->db->update('mahasiswa',$data);
    

            $this->set_response($data, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
        }

        else{
            $message = array("status" => "data tidak berhasil diupdate");
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST); 
        }
    }
}
