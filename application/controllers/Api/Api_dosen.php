<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Api/Auth.php';

use Restserver\Libraries\REST_Controller;


class Api_dosen extends Auth
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_get()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL) {
            $users = $this->db->get('dosen')->result_array();
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users) {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        //Find and return a single record for a particular user.
        else {
            $id = (int) $id;

            // Validate the id.
            if ($id <= 0) {
                // Invalid id, set the response and exit.
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            $this->db->where(array("id" => $id));
            $users = $this->db->get('dosen')->row_array();

            // Set the response and exit
            $this->response($users, REST_Controller::HTTP_OK);
        }
    }

    public function index_post()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        // $this->some_model->update_user( ... );
        $id_group = 3;
        $nidn = $this->input->post('nidn');
        $password = hash("sha256", $nidn);


        $data_akun = [
            "username" => $this->input->post('nidn'),
            "password" => $password,
            "real_name" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "id_group" => $id_group,
        ];
        $this->db->insert('user', $data_akun);
        $last_id_user = $this->db->insert_id();

        $data_dosen = [
            "nidn" => $this->input->post('nidn'),
            "nama" => $this->input->post('nama'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "no_hp" => $this->input->post('no_hp'),
            "email" => $this->input->post('email'),
            "id_user" => $last_id_user,
            "alamat" => $this->input->post('alamat'),
        ];
        $this->db->insert('dosen', $data_dosen);

        $this->set_response($data_dosen, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }


    public function index_delete()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        $id = $this->delete('id');
        // print_r($id);exit;
        $cek =  $this->db->get_where('dosen', ['id' => $id])->num_rows();

        // print_r($cek);exit;
        // Validate the id.

        if ($cek > 0) {
            // Set the response and exit
            $where = [
                'id' => $id
            ];
            $this->db->where('id', $id);
            $this->db->delete('dosen');
            $this->db->delete('dosen', $where);
            $message = array("status" => "data berhasil dihapus");

            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {

            $message = array("status" => "data tidak berhasil dihapus");
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function index_put()
    {
        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        $id = $this->put('id');

        $cek =  $this->db->get_where('dosen', ['id' => $id])->num_rows();
        // print_r($id);exit;
        if ($cek > 0) {


            $data_dosen = [
                "nidn" => $this->put('nidn'),
                "nama" => $this->put('nama'),
                "jenis_kelamin" => $this->put('jenis_kelamin'),
                "no_hp" => $this->put('no_hp'),
                "email" => $this->put('email'),
                "alamat" => $this->put('alamat'),
            ];

            $this->db->where('id', $id);
            $this->db->update('dosen', $data_dosen);


            $this->set_response($data_dosen, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
        } else {
            $message = array("status" => "data tidak berhasil diupdate");
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
