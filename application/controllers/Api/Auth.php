<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/JWT.php';
require APPPATH . '/libraries/Key.php';
require APPPATH . '/libraries/ExpiredException.php';
require APPPATH . '/libraries/BeforeValidException.php';
require APPPATH . '/libraries/SignatureInvalidException.php';
require APPPATH . '/libraries/JWK.php';
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;
// use chriskacerguis\RestServer\Rest_Controller;
// use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;
use \Firebase\JWT\Key;


class Auth extends REST_Controller
{


    public function configToken()
    {
        $cnf['exp'] = 3600; //milisecond
        $cnf['secretkey'] = '2212336221';
        return $cnf;
    }

    public function authtoken()
    {

        if (array_key_exists('Authorization', $this->input->request_headers()) == false) { //kalo gadak key token

            $data = array('status' => false, 'messsage' => 'token tidak ada');
            return $this->response('salah', 401);


            die();
        }

        $secret_key = $this->configToken()['secretkey'];
        $token = null;
        $authHeader = $this->input->request_headers()['Authorization'];
        $arr = explode(" ", $authHeader);
        $token = $arr[1];
        if ($token) {
            try {
                $decoded = JWT::decode($token, new Key($secret_key, 'HS256'));
                if ($decoded) {
                    return 'benar';
                }
            } catch (\Exception $e) {
                $result = array('pesan' => 'Kode Signature Tidak Sesuai');
                return 'salah';
            }
        }
    }

    public function index_get()
    {

        if ($this->authtoken() == 'salah') {
            return $this->response(array('kode' => '401', 'pesan' => 'signature tidak sesuai', 'data' => []), '401');
            die();
        }
        $this->db->select('*');
        $data = array('data' => $this->db->get('mahasiswa')->result());
        $this->response($data, 200);
    }



    public function index_post()
    {


        $username = $this->post('username');
        $password = $this->post('password');
        $where = array(
            'username' => $username,
            'password' => hash("sha256", $password)
        );
        $cek = $this->M_mahasiswa->cek_login('user', $where)->num_rows();

        $cek2 = $this->db->get_where('user', $where)->row_array();

       
        // print_r($id_group);exit;
       


            if ($cek > 0) {
                $id_group = $cek2['id_group'];
                if ($id_group == 1) {
                    
                $data_session = array(
                    'nama' => $username,
                    'status' => "login"
                );
                $exp = time() + 3600;
                $token = array(
                    "iss" => 'apprestservice',
                    "aud" => 'pengguna',
                    "iat" => time(),
                    "nbf" => time() + 10,
                    "exp" => $exp,
                    "data" => array(
                        "username" => $this->post('username'),
                        "password" => $this->post('password')
                    )


                );
                $jwt = JWT::encode($token, $this->configToken()['secretkey'], 'HS256');
                $output = [
                    'status' => 200,
                    'message' => 'Berhasil login',
                    "token" => $jwt,
                    "expireAt" => $token['exp']
                ];
                $data = array('kode' => '200', 'pesan' => 'token', 'data' => array('token' => $jwt, 'exp' => $exp));
                $this->response($data, 200);
                }
                else{
                    $this->response('Anda bukan admin', 404);
                }
            }
        
            else {
                $this->response('username atau password salah', 404);
            }
        
    }
}
