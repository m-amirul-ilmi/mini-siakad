<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
            redirect(base_url('login'));
        }
    }

	public function index()
	{
		$title['title'] = 'Mini Siakad';

		$data = array(
			'mahasiswa' => $this->M_mahasiswa->getdata(),
		);
		// print_r($data);exit; 
		$this->load->view('templates/header',$title);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('templates/index',$data);
		$this->load->view('templates/footer');
		
	}

	public function halaman_mahasiswa()
	{
		$title['title'] = 'Halaman Mahasiswa';
		$data = array(
			'mahasiswa' => $this->M_mahasiswa->getdata(),
		);
		// print_r($data);exit; 
		$this->load->view('templates/header',$title);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_mahasiswa',$data);
		$this->load->view('templates/footer');
	}	

	public function halaman_dosen()
	{
		if ($this->session->userdata('id_group')<=3 ) {
		$dataa['title'] = 'Halaman Dosen';
		$data['dosen'] = $this->M_mahasiswa->getdata_dosen();
		$this->load->view('templates/header',$dataa);
		$this->load->view('templates/sidebar',$data);
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_dosen',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}	

	public function halaman_BAAK()
	{
		if ($this->session->userdata('id_group')==1 ) {
		$data_title['title'] = 'Halaman Admin BAAK';
		$data['baak'] = $this->M_mahasiswa->getdata_baak();
		$this->load->view('templates/header',$data_title);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_baak',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}	

	public function halaman_mahasiswa_bimbingan()
	{
		$data['title'] = 'Halaman Mahasiswa Bimbingan';
		$data_dosen = $this->M_mahasiswa->getdata_dosen_bimbingan();
		
		$id_dosen = $data_dosen['id'];
		
		// $data_mahasiswa['mahasiswa'] = $this->M_mahasiswa->getdata_mahasiswa_bimbingan($id_dosen);
		
		$data_mahasiswa = array(
			'mahasiswa' => $this->M_mahasiswa->getdata(),
			'mahasiswaa' => $this->M_mahasiswa->getdata_mahasiswa_bimbingan($id_dosen)
		);
		// print_r($data_mahasiswa_detail);exit;
		
		$this->load->view('templates/header',$data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_mahasiswa_bimbingan',$data_mahasiswa);
		$this->load->view('templates/footer');
	}

	public function halaman_tambah()
	{
		if ($this->session->userdata('id_group')<=2 ) {
		$data['title'] = 'Halaman tambah data';
		$data['mahasiswa'] = $this->M_mahasiswa->getdata();
		
		$this->load->view('templates/header',$data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_tambah',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}
	
	public function halaman_tambah_dosen()
	{
		if ($this->session->userdata('id_group')<=2 ) {
		$data['title'] = 'Halaman Tambah Data Dosen';
		$this->load->view('templates/header',$data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_tambah_dosen',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}

	public function halaman_tambah_baak()
	{
		if ($this->session->userdata('id_group')==1 ) {
		$data['title'] = 'Halaman Tambah Data BAAK';
		$this->load->view('templates/header',$data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_tambah_baak',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}


	public function halaman_edit($id,$id_orangtua,$id_dosen)
	{
		$dataa['title'] = 'Halaman edit data';
		// $mahasiswa = $this->M_mahasiswa->mahasiswa_detail($id);
		// print_r($mahasiswa);exit;
		$data = array(
			'mahasiswa' => $this->M_mahasiswa->mahasiswa_detail($id),
			'orangtua' => $this->M_mahasiswa->orangtua_detail($id_orangtua),
			'dosen' => $this->M_mahasiswa->dosen_detail($id_dosen),//cuman untuk ngambil nidn
			
				);
		// print_r($data);exit;

		$this->load->view('templates/header',$dataa);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_edit',$data);
		$this->load->view('templates/footer');
	}

	public function halaman_edit_dosen($id)
	{
		if ($this->session->userdata('id_group')<=2 ) {
		$dataa['title'] = 'Halaman Edit Data Dosen';
		
		$data = array(
			'dosen' => $this->M_mahasiswa->dosen_detail($id),
		);

		$this->load->view('templates/header',$dataa);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_edit_dosen',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}

	public function halaman_edit_baak($id)
	{
		if ($this->session->userdata('id_group')==1 ) {
		$dataa['title'] = 'Halaman Edit Data Admin BAAK';
		
		$data = array(
			'baak' => $this->M_mahasiswa->baak_detail($id),
		);
		// print_r($data);exit;

		$this->load->view('templates/header',$dataa);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_edit_baak',$data);
		$this->load->view('templates/footer');
		}
		else{
			redirect();
		}
	}

	public function proses_tambah_data()
	{	
		$nidn = $this->input->post('nidn');
		$data_dosen = $this->M_mahasiswa->getdata_nidn($nidn);
		// print_r($data_dosen);exit;
		$this->M_mahasiswa->proses_tambah_data($data_dosen);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data baru berhasil ditambahkan!</div>');
		redirect(base_url('home/halaman_mahasiswa'));

	}
	public function proses_tambah_data_dosen()
	{	
		$this->M_mahasiswa->proses_tambah_data_dosen();
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data baru berhasil ditambahkan!</div>');
		redirect(base_url('home/halaman_dosen'));

	}
	public function proses_tambah_data_baak()
	{	
		$this->M_mahasiswa->proses_tambah_data_baak();
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data baru berhasil ditambahkan!</div>');
		redirect(base_url('home/halaman_baak'));

	}

	public function proses_edit_data()
	{
		$id_orangtua = $this->input->post('id_orangtua');//inputan post hidden

		$nidn = $this->input->post('nidn');
		$data_dosen = $this->M_mahasiswa->getdata_nidn($nidn);

		$data_mahasiswa = array(
			'nim' => $this->input->post('nim'),
			'id_dosen' => $data_dosen['id'],
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'alamat' => $this->input->post('alamat'),
			'no_hp' => $this->input->post('no_hp')
		);

		$data_orangtua = array(
			'nama_ayah' => $this->input->post('nama_ayah'),
			'no_hp_ayah' => $this->input->post('no_hp_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'no_hp_ibu' => $this->input->post('no_hp_ibu'),
			'alamat_ortu' => $this->input->post('alamat_ortu')	
		);

		$this->M_mahasiswa->proses_edit_data($id_orangtua,$data_mahasiswa,$data_orangtua);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data berhasil diedit!</div>');
		redirect(base_url('home/halaman_mahasiswa'));
	}

	public function proses_edit_dosen()
	{
		$id = $this->input->post('id');//inputan post hidden
		$data_dosen = array(
			'nidn' => $this->input->post('nidn'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'no_hp' => $this->input->post('no_hp'),
			'email' => $this->input->post('email'),
			'alamat' => $this->input->post('alamat'),
			
		);

		$this->M_mahasiswa->proses_edit_dosen($id,$data_dosen);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data berhasil diedit!</div>');
		redirect(base_url('home/halaman_dosen'));
	}

	public function proses_edit_baak()
	{
		$id = $this->input->post('id');//inputan post hidden
		$data_baak = array(
			'username' => $this->input->post('username'),
			'real_name' => $this->input->post('real_name'),
			'email' => $this->input->post('email'),
			
		);

		$this->M_mahasiswa->proses_edit_baak($id,$data_baak);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data berhasil diedit!</div>');
		redirect(base_url('home/halaman_baak'));
	}

	public function fungsi_hapus($id,$id_orangtua,$id_user)
	{
		if ($this->session->userdata('id_group')==1 ) {
		$this->M_mahasiswa->fungsi_hapus($id,$id_orangtua,$id_user);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		redirect(base_url('home/halaman_mahasiswa'));
		}
	}
	public function fungsi_hapus_dosen($id,$id_user)
	{
		if ($this->session->userdata('id_group')==1 ) {
		$this->M_mahasiswa->fungsi_hapus_dosen($id,$id_user);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		redirect(base_url('home/halaman_dosen'));
		}
	}
	public function fungsi_hapus_baak($id)
	{
		if ($this->session->userdata('id_group')==1 ) {
		$this->M_mahasiswa->fungsi_hapus_baak($id);
		$this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		redirect(base_url('home/halaman_baak'));
		}
	}

	public function pdf($id,$id_orangtua){

		$data = array(
			'mahasiswa' => $this->M_mahasiswa->mahasiswa_detail($id),
			'orangtua' => $this->M_mahasiswa->orangtua_detail($id_orangtua),
		);
		// print_r($data);exit;
		// $data = null;
		// print_r($data);exit;
		$this->load->library('pdf');
	
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', true);
		$this->pdf->filename = "laporan-petanikode.pdf";
		$this->pdf->load_view('home/laporan_pdf', $data);
		
	
	}

	public function halaman_film(){

		$title['title'] = 'Mini Siakad';
		$this->load->view('templates/header',$title);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar');
		$this->load->view('home/halaman_film');
		$this->load->view('templates/footer');
	}
	
}
