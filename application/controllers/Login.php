<?php  

class Login extends CI_Controller{

    public function index()
    {
		$vals = [
			// substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 8),
            'word'          => "tes",
            'img_path'      => './images/',
            'img_url'       => base_url('images/'),
            'img_width'     => '150',
            'img_height'    => 40,
            'expiration'    => 8200,
            'word_length'   => 8,
            'font_size'     => 16,
            'img_id'        => 'Imageid',
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    
            'colors'        => [
                    'background'=> [255, 255, 255],
                    'border'    => [255, 255, 255],
                    'text'      => [0, 0, 0],
                    'grid'      => [255, 40, 40]
                ]
            ];
        
        $captcha = create_captcha($vals);
        $this->session->set_userdata('captcha', $captcha['word']);
        $this->load->view('home/halaman_login.php', ['captcha' => $captcha['image']]);
		$captcha    = $this->session->userdata('captcha');
    }

    public function aksi_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => hash("sha256",$password)
			);
		$cek = $this->M_mahasiswa->cek_login('user',$where)->num_rows();

		//pengambilan data-data penting di dalam table user
		$cek_profil = $this->M_mahasiswa->cek_login('user',$where)->row();
		$data_profil = array(
		'id_group' => $cek_profil->id_group,//untuk membatasi hak akses
		'id_user'  => $cek_profil->id,//untuk menampilkan edit untuk dirinya sendiri di mahasiswa
		'username' => $cek_profil->username//untuk menampilkan nama profil sesuai username masuk
		);

		if($cek > 0){
			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);

			
			
			
			// cek captcha
			$post_code  = $this->input->post('captcha');
			$captcha    = $this->session->userdata('captcha');
			if ($post_code && ($post_code == $captcha)) 
			{
				
				$this->session->set_userdata($data_session);
				$this->session->set_userdata($data_profil);
				redirect('home');
			}
			else
			{
				$this->session->set_flashdata('pesan_form', '<font style="color: red"><b>Captcha yang Anda ketik salah!</b></font><br/><br/>');
				redirect('login');
			}

		}
		else
		{
			$this->session->set_flashdata('pesan','<div class="alert alert-danger" role="alert">Username atau password salah!</div>');
			redirect(base_url('login'));
		}
		
	}
	public function halaman_register(){
		$this->load->view('home/halaman_register');
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

}
?>