<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"><?php echo $title; ?></h6>
                        <?php if ($this->session->userdata('id_group')<=2 ) { ?>
                        <a href="<?php echo base_url('home/halaman_tambah_dosen')?>" class="btn btn-primary btn-sm float-right">Tambah Data</a>
                        <?php }?>
                    </div>
                    
                    <div class="card-body">
                        <?php echo $this->session->flashdata('pesan')?>
                        <div class="table-responsive">
                            <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIDN</th>
                                    <th>Nama</th>
                                    <th>Jenis Kelamin</th>
                                    <th>NO HP</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                    
                                    
                                </tr>
                                </thead>
                                <tbody>   
                                    <?php 
                                    $no = 1;
                                        foreach ($dosen as $dsn) {?>
                                            <tr>
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $dsn['nidn']; ?></td>
                                                <td><?php echo $dsn['nama']; ?></td>
                                                <td><?php echo $dsn['jenis_kelamin']; ?></td>
                                                <td><?php echo $dsn['no_hp']; ?></td>
                                                <td><?php echo $dsn['email']; ?></td>
                                                <td><?php echo $dsn['alamat']; ?></td>
                                               
                                                
                                                <td>
                                                <a type="button" class="badge badge-success" data-toggle="modal" data-target="#exampleModal<?php echo $dsn['id'];?>">Detail</a>
                                                <?php if ($this->session->userdata('id_user')==$dsn['id_user'] ) {  ?>
                                                <a type="button" class="badge badge-success" data-toggle="modal" data-target="#exampleModal<?php echo $dsn['id'];?>">Detail</a>
                                                <?php } ?>

                                                    <?php if ($this->session->userdata('id_group')<=2 ) { ?>
                                                    <a href="<?php echo base_url('home/halaman_edit_dosen')?>/<?php echo $dsn['id'];?>" class="badge badge-primary">Edit</a>

                                                        <?php if ($this->session->userdata('id_group')==1 ) { ?>
                                                        <a href="<?php echo base_url('home/fungsi_hapus_dosen')?>/<?php echo $dsn['id'];?>/<?php echo $dsn['id_user'];?>" class="badge badge-danger">Hapus</a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                        <?php } ?>
                                </tbody>
                            </table>
               </div>
              </div>
             </div>
            </div>


<!-- Modal -->
<?php foreach ($dosen as $dsn) {?>

<div class="modal fade" id="exampleModal<?php echo $dsn['id'];?>"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Dosen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
        <table class="table table-bordered no-margin">
          <tr>
            <tr> 
            <tr>  
              <th>NIDN </th>
              <td><?php echo $dsn['nidn']; ?></td>
            </tr> 
              <th>Nama </th>
              <td><?php echo $dsn['nama']; ?></td>
            </tr>
            <tr>  
              <th>Jenis Kelamin </th>
              <td><?php echo $dsn['jenis_kelamin']; ?></td>
            </tr>
            </tr> 
              <th>NO HP </th>
              <td><?php echo $dsn['no_hp']; ?></td>
            </tr>
              <th>Email </th>
              <td><?php echo $dsn['email']; ?></td>
            </tr>
            <tr>  
              <th>Alamat</th>
              <td><?php echo $dsn['alamat']; ?></td>
            </tr>
            
           
          </tr>

          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<!--Akhir Modal -->