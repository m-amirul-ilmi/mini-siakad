<div class="container-fluid">
    <h3>Halaman Edit Data Dosen</h3>
    <hr>
    <br>

    
    <form method="POST" action="<?php echo base_url('/home/proses_edit_dosen') ?>">
    <?php $id_group = 4; ?>
    <input type="hidden" name="id" value="<?php echo $dosen['id'];?>">
        <div class="row mb-3">
            <label for="nim" class="col-sm-2 col-form-label">NIDN</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nidn" value="<?php echo $dosen['nidn']?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="nim" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nama" value="<?php echo $dosen['nama']?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-5">
                <select class="form-control" aria-label="Default select example" name="jenis_kelamin" value="<?php echo $dosen['jenis_kelamin'];?>">
                    <option selected><?php echo $dosen['jenis_kelamin'];?></option>
                    <?php if($dosen['jenis_kelamin']=='perempuan'){ ?>
                    <option>laki-laki</option>
                    <?php } ?>

                    <?php if($dosen['jenis_kelamin']=='laki-laki'){ ?>
                    <option>perempuan</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">NO HP</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="no_hp" value="<?php echo $dosen['no_hp']?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="email" value="<?php echo $dosen['email']?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="alamat" value="<?php echo $dosen['alamat']?>">
            </div>
        </div>



        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-5">
                <button type="submit" class="btn btn-primary">Ubah</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </div>
        
    </form>
</div>