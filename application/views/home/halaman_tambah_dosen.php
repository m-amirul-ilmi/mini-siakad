<div class="container-fluid">
    <h3>Halaman Tambah Data Dosen</h3>
    <hr>
    <br>

    <form method="POST" action="<?php echo base_url('/home/proses_tambah_data_dosen') ?>">
    <?php $id_group = 3; ?>
    <input type="hidden" name="id_group" value="<?php echo $id_group;?>">
        <div class="row mb-3">
            <label for="nim" class="col-sm-2 col-form-label">NIDN</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nidn">
            </div>
        </div>
        <div class="row mb-3">
            <label for="nim" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nama">
            </div>
        </div>
        <div class="row mb-3">
            <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-5">
                <select class="form-control" aria-label="Default select example" name="jenis_kelamin">
                    <option selected>laki-laki</option>
                    <option>perempuan</option>
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">NO HP</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="no_hp">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="email">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="alamat">
            </div>
        </div>



        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-5">
                <button type="submit" class="btn btn-primary">Tambah</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </div>
        
    </form>
</div>