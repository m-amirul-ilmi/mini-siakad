<div class="container-fluid">
    <h3>Halaman Edit data</h3>
    <hr>
    <br>
    <form method="POST" action="<?php echo base_url('/home/proses_edit_data') ?>">

   
        <h4>Data Mahasiswa : </h4>
        <br>
        <input type="hidden" name="id" value="<?php echo $mahasiswa['id'];?>">
        <div class="row mb-3">
            <label for="nama" class="col-sm-2 col-form-label" >NIM</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nim" value="<?php echo $mahasiswa['nim'];?>" readonly>
            </div>
        </div>
        <div class="row mb-3">
            <label for="nim" class="col-sm-2 col-form-label">NIDN Dosen</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nidn" value="<?php echo $dosen['nidn'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="nim" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nama" value="<?php echo $mahasiswa['nama'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="email" value="<?php echo $mahasiswa['email'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-5">
                <select class="form-control" aria-label="Default select example" name="jenis_kelamin" value="<?php echo $mahasiswa['jenis_kelamin'];?>">
                    <option selected><?php echo $mahasiswa['jenis_kelamin'];?></option>
                    <?php if($mahasiswa['jenis_kelamin']=='perempuan'){ ?>
                    <option>laki-laki</option>
                    <?php } ?>

                    <?php if($mahasiswa['jenis_kelamin']=='laki-laki'){ ?>
                    <option>perempuan</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="alamat" value="<?php echo $mahasiswa['alamat'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">No HP</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="no_hp" value="<?php echo $mahasiswa['no_hp'];?>">
            </div>
        </div>

        <br>
        <h4>Data Orangtua : </h4>
        <br>
        <input type="hidden" name="id_orangtua" value="<?php echo $orangtua['id_orangtua'];?>">
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Nama Ayah</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nama_ayah" value="<?php echo $orangtua['nama_ayah'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">No HP Ayah</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="no_hp_ayah" value="<?php echo $orangtua['no_hp_ayah'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Nama Ibu</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="nama_ibu" value="<?php echo $orangtua['nama_ibu'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">No HP Ibu</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="no_hp_ibu" value="<?php echo $orangtua['no_hp_ibu'];?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-5">
            <input type="text" class="form-control" name="alamat_ortu" value="<?php echo $orangtua['alamat_ortu'];?>">
            </div>
        </div>


        <div class="row mb-3">
            <label for="alamat" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-5">
                <button type="submit" class="btn btn-primary">Ubah</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </div>
    </form>
</div>