<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"><?php echo $title; ?></h6>
                        <?php if ($this->session->userdata('id_group')<=2 ) { ?>
                          <a href="<?php echo base_url('home/halaman_tambah')?>" class="btn btn-primary btn-sm float-right">Tambah Data</a>  
                        <?php }?>
                    </div>
                    <div class="card-body">
                        <?php echo $this->session->flashdata('pesan')?>
                        <div class="table-responsive">
                            <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NIM</th>
                                    <th>Email</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Alamat</th>
                                    <th>No HP</th>
                                    <th>Aksi</th>        
                                </tr>
                                </thead>
                                <tbody>   
                                    <?php 
                                    $no = 1;
                                        foreach ($mahasiswa as $mhs) {?>
                                            <tr>
                                               
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $mhs['nama']; ?></td>
                                                <td><?php echo $mhs['nim']; ?></td>
                                                <td><?php echo $mhs['email']; ?></td>
                                                <td><?php echo $mhs['jenis_kelamin']; ?></td>
                                                <td><?php echo $mhs['alamat']; ?></td>
                                                <td><?php echo $mhs['no_hp']; ?></td>
                                                
                                                
                                                <td>
                                                    <?php if ($this->session->userdata('id_group')<=3 ) { ?>
                                                        <a type="button" class="badge badge-success" data-toggle="modal" data-target="#exampleModal<?php echo $mhs['id'];?>">Detail</a>
                                                    <?php } ?>

                                                    <?php if ($this->session->userdata('id_user')==$mhs['id_user'] ) {  ?>
                                                      <a type="button" class="badge badge-success" data-toggle="modal" data-target="#exampleModal<?php echo $mhs['id'];?>">Detail</a>
                                                      <a href="<?php echo base_url('home/halaman_edit')?>/<?php echo $mhs['id'];?>/<?php echo $mhs['id_orangtua'];?>/<?php echo $mhs['id_dosen'];?>" class="badge badge-primary">Edit</a>
                                                      <a href="<?php echo base_url('home/pdf')?>/<?php echo $mhs['id'];?>/<?php echo $mhs['id_orangtua'];?>" class="badge badge-warning">Print</a>
                                                    <?php } ?>

                                                    <?php if ($this->session->userdata('id_group')<=2 ) { ?>
                                                    <a href="<?php echo base_url('home/halaman_edit')?>/<?php echo $mhs['id'];?>/<?php echo $mhs['id_orangtua'];?>/<?php echo $mhs['id_dosen'];?>" class="badge badge-primary">Edit</a>
                                                    

                                                        <?php if ($this->session->userdata('id_group')==1 ) { ?>
                                                        <a href="<?php echo base_url('home/fungsi_hapus')?>/<?php echo $mhs['id'];?>/<?php echo $mhs['id_orangtua'];?>/<?php echo $mhs['id_user'];?>" class="badge badge-danger">Hapus</a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                   
                                                
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        
                                       
                                </tbody>
                            </table>
               </div>
              </div>
             </div>
            </div>


<!-- Modal -->
<?php foreach ($mahasiswa as $mhs) {?>

<div class="modal fade" id="exampleModal<?php echo $mhs['id'];?>"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Mahasiswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
        <table class="table table-bordered no-margin">
          <tr>
            <tr>  
              <th>Nama </th>
              <td><?php echo $mhs['nama']; ?></td>
            </tr>
            <tr>  
              <th>NIM </th>
              <td><?php echo $mhs['nim']; ?></td>
            </tr>
            <tr>  
              <th>Email </th>
              <td><?php echo $mhs['email']; ?></td>
            </tr>
            <tr>  
              <th>Jenis Kelamin </th>
              <td><?php echo $mhs['jenis_kelamin']; ?></td>
            </tr>
            <tr>  
              <th>Alamat</th>
              <td><?php echo $mhs['alamat']; ?></td>
            </tr>
            <tr>  
              <th>NO HP </th>
              <td><?php echo $mhs['no_hp']; ?></td>
            </tr>
            <tr>  
              <th>Nama Ayah </th>
              <td><?php echo $mhs['nama_ayah']; ?></td>
            </tr>
            <tr>  
              <th>NO HP Ayah </th>
              <td><?php echo $mhs['no_hp_ayah']; ?></td>
            </tr>
            <tr>  
              <th>Nama Ibu </th>
              <td><?php echo $mhs['nama_ibu']; ?></td>
            </tr>
            <tr>  
              <th>NO HP Ibu </th>
              <td><?php echo $mhs['no_hp_ibu']; ?></td>
            </tr>
            <tr>  
              <th>Alamat Orangtua </th>
              <td><?php echo $mhs['alamat_ortu']; ?></td>
            </tr>
            
           
           
          </tr>

          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<!-- Akhir Modal -->
        