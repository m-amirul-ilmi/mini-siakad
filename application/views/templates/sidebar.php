<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SIAKAD</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider">
<div class="sidebar-heading">
    Administrator
</div>

<!-- Nav Item - Dashboard -->
<li class="nav-item">
    <a class="nav-link" href="<?php echo base_url('home')?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>


<!-- Divider -->
<hr class="sidebar-divider">


<?php if ($this->session->userdata('id_group')<=2 ) { ?>
<!-- Heading -->
<div class="sidebar-heading">
    Addons
</div>

<!-- Nav Item - Tables -->
<li class="nav-item">
    <a class="nav-link" href="<?php echo base_url('home/halaman_mahasiswa')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Halaman Mahasiswa</span></a>
    <a class="nav-link" href="<?php echo base_url('home/halaman_dosen')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Halaman Dosen</span></a>
    <?php if ($this->session->userdata('id_group')==1 ) { ?>
    <a class="nav-link" href="<?php echo base_url('home/halaman_BAAK')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Halaman Admin BAAK</span></a>
    <a class="nav-link" href="<?php echo base_url('home/halaman_film')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Halaman Film</span></a>
    <?php }?>
    <hr>
</li>
<?php }?>

<?php if ($this->session->userdata('id_group')==3 ) { ?>
<!-- Heading -->
<div class="sidebar-heading">
    Addons
</div>

<!-- Nav Item - Tables -->
<li class="nav-item">
    <!-- <a class="nav-link" href="<?php echo base_url('home/halaman_mahasiswa')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Data Mahasiswa</span></a> -->
   
        <a class="nav-link" href="<?php echo base_url('home/halaman_dosen')?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Halaman Dosen</span></a>
   
    <a class="nav-link" href="<?php echo base_url('home/halaman_mahasiswa_bimbingan')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Mahasiswa Bimbingan</span></a>
       <hr>
</li>
<?php }?>

<?php if ($this->session->userdata('id_group')==4 ) { ?>
<!-- Heading -->
<div class="sidebar-heading">
    Addons
</div>

<!-- Nav Item - Tables -->
<li class="nav-item">
    <a class="nav-link" href="<?php echo base_url('home/halaman_mahasiswa')?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Data Mahasiswa</span></a>
</li>
<?php }?>

<!-- Heading -->
<div class="sidebar-heading">
    Interface
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item active">
                <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
                    aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Pages</span>
                </a>
                <div id="collapsePages" class="collapse show" aria-labelledby="headingPages"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <a class="collapse-item" href="<?php echo base_url('login')?>">Login</a>

                        <?php if ($this->session->userdata('id_group')<=2 ) { ?>
                        <a class="collapse-item" href="<?php echo base_url('login/halaman_register') ?>">Register</a>
                        <?php }?>

                        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <a class="collapse-item" href="<?php echo base_url('login/logout') ?>">Logout</a>
                        <a class="collapse-item active" href="blank.html">Blank Page</a>
                    </div>
                </div>
            </li>


<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->